from .base import *
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
ALLOWED_HOSTS = []

INSTALLED_APPS += ()

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': os.environ['DB_COMUNACUERDO'],
        'USER': os.environ['DB_USER'],
        'PASSWORD': os.environ['DB_PASS']
    }
}