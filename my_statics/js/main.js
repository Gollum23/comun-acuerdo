(function() {
    "use strict";

    $('#Form-subscribe').on('submit', send_mail);

    scaleVideoContainer();

    initBannerVideoSize('.Video-container .poster img');
    initBannerVideoSize('.Video-container .Video-filter');
    initBannerVideoSize('.Video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.Video-container .poster img');
        scaleBannerVideoSize('.Video-container .Video-filter');
        scaleBannerVideoSize('.Video-container video');
    });


    function scaleVideoContainer() {

        var height = $(window).height();
        var unitHeight = parseInt(height) + 'px';
        $('.Container').css('height',unitHeight);

    }

    function initBannerVideoSize(element){

        $(element).each(function(){
            $(this).data('height', $(this).height());
            $(this).data('width', $(this).width());
        });

        scaleBannerVideoSize(element);
    }

    function scaleBannerVideoSize(element){

        var windowWidth = $(window).width(),
        windowHeight = $(window).height() + 5,
        videoWidth,
        videoHeight;

        $(element).each(function(){
            var videoAspectRatio = $(this).data('height')/$(this).data('width');

            $(this).width(windowWidth);

            if(windowWidth < 1000){
                videoHeight = windowHeight;
                videoWidth = videoHeight / videoAspectRatio;
                $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

                $(this).width(videoWidth).height(videoHeight);
            }

            $('.Container .Video-container video').addClass('fadeIn animated');

        });
    }

    function send_mail(ev) {
        ev.preventDefault();
        if ($('#id_name').val()!= '' || $('#id_email').val()!='') {
            $('#Form-subscribe').find('.Btn i').removeClass('fa-send').addClass('fa-spinner fa-pulse');
            $.ajax({
                url: '/',
                method: 'POST',
                data: $('#Form-subscribe').serializeArray()
            })
                .done(function(data) {
                    $('.Message').html(data.message).fadeIn().delay(5000).fadeOut();
                    $('#id_name').val('');
                    $('#id_email').val('');
                    $('#id_organization').val('');
                    $('#id_town').val('');
                    $('#Form-subscribe').find('.Btn i').removeClass('fa-spinner fa-pulse').addClass('fa-send');
                });
        }
        else {
            $('.Message').html('<p>Debe ingresar su nombre y un correo electrónico valido</p>').fadeIn().delay(3000).fadeOut();
        }

    }

})();