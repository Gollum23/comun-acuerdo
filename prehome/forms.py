from django import forms
from .models import Subscribe


class SubscribeForm(forms.ModelForm):

    class Meta:
        model = Subscribe
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Nombre completo'}),
            'email': forms.EmailInput(attrs={'placeholder': 'Correo electrónico'}),
            'organization': forms.TextInput(attrs={'placeholder': 'Organización'}),
            'town': forms.TextInput(attrs={'placeholder': 'Ciudad/Municipio'})
        }
        fields = '__all__'
