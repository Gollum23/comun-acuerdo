# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Subscribe',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('name', models.CharField(verbose_name='Nombre completo', max_length=255)),
                ('email', models.EmailField(verbose_name='Correo electrónico', max_length=254)),
                ('organization', models.CharField(blank=True, verbose_name='Organización', max_length=255)),
                ('town', models.CharField(blank=True, verbose_name='Ciudad/Municipio', max_length=100)),
                ('date_subscribe', models.DateField(auto_now_add=True)),
            ],
        ),
    ]
