from django.conf.urls import url

from .views import PreHome

urlpatterns = [
    url(r'^$', PreHome.as_view(), name='prehome'),
]
