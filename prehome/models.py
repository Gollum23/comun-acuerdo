from django.db import models


class Subscribe(models.Model):
    name = models.CharField(
        verbose_name='Nombre completo',
        max_length=255
    )
    email = models.EmailField(
        verbose_name='Correo electrónico',
    )
    organization = models.CharField(
        verbose_name='Organización',
        max_length=255,
        blank=True
    )
    town = models.CharField(
        verbose_name='Ciudad/Municipio',
        max_length=100,
        blank=True
    )
    date_subscribe = models.DateField(
        auto_now_add=True
    )

    def __str__(self):
        return '{}, {}'.format(self.name, self.organization)
