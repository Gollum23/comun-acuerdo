from django.core.mail import send_mail
from django.core.urlresolvers import reverse_lazy
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.views.generic import TemplateView, FormView

from .forms import SubscribeForm


class PreHome(FormView):
    form_class = SubscribeForm
    template_name = 'home.html'
    success_url = '/'

    def get_context_data(self, **kwargs):
        context = super(PreHome, self).get_context_data(**kwargs)
        context['form'] = self.form_class
        return context

    def form_valid(self, form):
        if self.request.is_ajax():
            super(PreHome, self).form_valid(form)
            f=form.save()
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            #endpoint = ['comunacuerdo-subscribe@lists.riseup.net']
            #endpoint = ['yiya.gomez@viva.org.co']
            endpoint = ['comunacuerdo@viva.org.co']
            subject = 'Suscripción comun acuerdo'
            body = render_to_string('email.txt', {'email': f})
            body_html = render_to_string('email.html', {'email': f})
            send_mail(subject, body, email, endpoint, html_message=body_html)
            message = 'Gracias, muy pronto nos pondremos en contacto contigo'
            data = {'message': message}
            return JsonResponse(data, safe=False)

